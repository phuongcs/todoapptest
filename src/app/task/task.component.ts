import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { BackendService, Task, User } from "../backend.service";
import {take} from 'rxjs/operators';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  taskId: number;
  task: Task;
  users: User[] = [];
  selectedUserId: number;
  assignedUser: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private backend: BackendService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(async params => {
      this.task = await this.backend.task(Number(params.get('id'))).toPromise();
      this.backend.users().pipe(take(1)).subscribe(users => {
        this.users = users;
        if (this.task.assigneeId) {
          this.assignedUser = this.users.find(item => item.id === Number(this.task.assigneeId));
        }
      })
    });

  }

  assignTask() {
    if (!this.selectedUserId) {
      alert('Please select user');
      return;
    }

    this.backend.assign(this.task.id, this.selectedUserId).subscribe(() => {
      this.assignedUser = this.users.find(item => item.id === Number(this.selectedUserId));
      alert('Assign user successfully!')
    })
  }

  completeTask() {
    this.backend.complete(this.task.id, true).subscribe(() => {
      this.router.navigate(['/']);
    });
  }
}
