import { Component, OnInit } from '@angular/core';
import { BackendService, Task } from "../backend.service";
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tasks: Task[]  = [];

  taskDescription: string = '';

  taskId = (index, task) => {
    return task.id;
  };

  private originalTask: Task[] = [];

  constructor(
    private backend: BackendService
  ) { }

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks() {
    this.backend.tasks().pipe(take(1)).subscribe(tasks => {
      this.originalTask = tasks.slice();
      this.tasks = tasks;
    });
  }

  addTask() {
    if (!this.taskDescription) {
      alert('Please input task description');
      return;
    }

    this.backend.newTask({description: this.taskDescription});
    this.getTasks();
  }

  filter(event) {
    if (event.target.checked) {
      this.tasks = this.originalTask.filter(item => !item.completed);
      return
    }

    this.tasks = this.originalTask.slice();
  }
}
